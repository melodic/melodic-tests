import net.schmizz.sshj.SSHClient
import net.schmizz.sshj.connection.channel.direct.Session
import net.schmizz.sshj.transport.verification.HostKeyVerifier
import net.schmizz.sshj.userauth.UserAuthException
import net.schmizz.sshj.userauth.keyprovider.KeyProvider
import net.sf.expectit.ExpectIOException
import static net.sf.expectit.matcher.Matchers.allOf
import static net.sf.expectit.matcher.Matchers.contains
import net.sf.expectit.Expect
import net.sf.expectit.ExpectBuilder
import static net.schmizz.sshj.connection.channel.direct.Session.Shell
import static net.sf.expectit.filter.Filters.removeColors
import static net.sf.expectit.filter.Filters.removeNonPrintable

import java.security.PublicKey
import java.util.concurrent.TimeUnit

import groovy.time.TimeCategory
import groovy.time.TimeDuration
import groovy.sql.Sql



class paasageService{

    boolean "servicesRestart"(){


        //load properties
        Properties properties = new Properties()
        File propertiesFile = new File('src/test/resources/melmac.properties')
        propertiesFile.withInputStream {
            properties.load(it)
        }

        String user = properties.getProperty("user")
        String password = properties.getProperty("password")
        String host = properties.getProperty("host.ip")
        int timeOutSeconds = properties.getProperty("time.out.in.seconds.restart").toInteger()

        boolean result = true
        boolean keyAuthentication = true
        final SSHClient ssh = new SSHClient()
        ssh.loadKnownHosts()
        ssh.addHostKeyVerifier(
                new HostKeyVerifier() {
                    boolean verify(String arg0, int arg1, PublicKey arg2) {
                        return true
                    }
                })


        ssh.setConnectTimeout(600)
        ssh.connect(host)

        File file = new File("src/test/resources/melodic_id_rsa")
        try {
            if (file.exists()) {
                System.out.println("Key authentication")
                KeyProvider keys = ssh.loadKeys(file.getPath())
                ssh.authPublickey(user, keys)
            } else {
                System.out.println("Password authentication")
                keyAuthentication = false
                ssh.authPassword(user, password)
            }


        final Session session = ssh.startSession()
        session.allocateDefaultPTY()
        Shell shell = session.startShell()
        Expect expect = new ExpectBuilder()
                .withOutput(shell.getOutputStream())
                .withInputs(shell.getInputStream(), shell.getErrorStream())
                .withEchoInput(System.out)
                .withEchoOutput(System.err)
                .withInputFilters(removeColors(), removeNonPrintable())
                .withExceptionOnFailure()
                .withTimeout(120, TimeUnit.SECONDS)
                .build()

        //Restart database
        def sql = Sql.newInstance("jdbc:mysql://$host","xxx","yyy","com.mysql.jdbc.Driver")
        sql.executeUpdate("drop database repo1")
        sql.executeUpdate("create database repo1")

        try {
            expect.sendLine("drestart\n")

            if (!keyAuthentication) {
                expect.expect(contains("password for ubuntu:"))
                expect.sendLine(password)
            }

            expect.expect(contains("Creating network"))

            for (int i = 0; i < 7; i++) { //8 serwisow
                expect.expect(contains("Creating service"))
                System.out.println("nowy serwis")
            }

            sleep(10000)//10s

            expect.sendLine("./cloudiator_reset.sh ")
            for (int i = 0; i < 2; i++) {
                expect.expect(contains("Waiting for tables creation..."))
            }


            result = false
            TimeDuration maxDuration = new TimeDuration(0,0,timeOutSeconds,0) //max czas oczekiwania - 4min
            Date startTime = new Date()

            while (!result && (TimeCategory.minus(new Date(),startTime)) <= maxDuration) {

                try {
                    expect.sendLine("mping\n")

                    expect.withTimeout(10,TimeUnit.SECONDS)expect(allOf(
                            contains("mule: 8088 status:  OK "),
                            contains("mule: 8089 status:  OK"),
                            contains("process: 8095 status:  OK"),
                            contains("adapter: 8097 status:  OK"),
                            contains("solver2deployment: 8096 status:  OK"),
                            contains("cdoserver: 2036 status:  OK"),
                            contains("generator: 8091 status:  OK"),
                            contains("cpsolver: 8093 status:  OK"),
                            contains("cloudiator: 9000 status:  OK"),
                            contains("memcache: 11211 status:  OK "),
                            contains("metasolver: 8092 status:  OK"),
                            contains("ds mq-1: 8161 status:  OK"),
                            contains("ds mq-2: 61616 status:  OK"),
                            contains("ds vms: 2222 status:  OK")))

                    result = true
                }catch(ExpectIOException expEx){
                    result = false
                }

            }



        }finally{
            System.out.println("Connection closed")
            expect.close()
            session.close()
            ssh.close()
        }
        }catch(UserAuthException uaeExc){
            if (keyAuthentication){
                System.out.println("Invalid publicKey")
            }
            else{
                System.out.println("Invalid password")
            }

            System.out.println(uaeExc.getMessage())
            return false
        }


        return result
    }
}
