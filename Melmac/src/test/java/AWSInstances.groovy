import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder
import com.amazonaws.services.ec2.model.AmazonEC2Exception
import com.amazonaws.services.ec2.model.DeleteKeyPairRequest
import com.amazonaws.services.ec2.model.DeleteKeyPairResult
import com.amazonaws.services.ec2.model.DescribeInstancesResult
import com.amazonaws.services.ec2.model.Instance
import com.amazonaws.services.ec2.model.Reservation
import com.amazonaws.services.ec2.model.TerminateInstancesRequest

//import javax.annotation.Generated

class AWSInstances {

    //@Generated(value="com.amazonaws:aws-java-sdk-code-generator")
    boolean clearInstances(){

        //read melmac.properties
        Properties properties = new Properties()
        File propertiesFile = new File('src/test/resources/melmac.properties')
        propertiesFile.withInputStream {
            properties.load(it)
        }

        String accessKey = properties.getProperty("access.key")
        String secretKey = properties.getProperty("secret.key")
        String region = properties.getProperty("region")

        Boolean clearInstancesSuccess = false
        AmazonEC2 ec2
        DescribeInstancesResult describeInstances

        try {
            BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey)

            ec2 = AmazonEC2ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                    .withRegion(region)
                    .build()


            describeInstances = ec2.describeInstances()

        }catch(AmazonEC2Exception ec2Ex){
            System.out.println("ACCESS_KEY or SECRET_KEY is incorrect\n" + ec2Ex.getMessage())
            return false
        }

        List <Reservation> reservations = describeInstances.getReservations()
        List<Instance> instances = new ArrayList<Instance>()

        for (Reservation reservation: reservations){
            for(Instance instance: reservation.getInstances()){
                if (instance.getState().getName() == "running" || instance.getState().getName() == "pending"){
                    instances.add(instance)
                }
            }
        }


        List<String> idsToRemove = new ArrayList<String>()
        List<String> keysToRemove = new ArrayList<String>()

        for (Instance instance: instances){
            String instanceId = instance.getInstanceId()
            String instanceName = instance.getTags()[0].getValue()
            String keyName = instance.getKeyName()

            if (instanceName.startsWith("aruba")){
                idsToRemove.add(instanceId)
                keysToRemove.add(keyName)
            }

        }

        if (!idsToRemove.empty) {//terminate instances

            System.out.println("To terminate: \n" + idsToRemove.toString())

            try {
                TerminateInstancesRequest terminatedRequest = new TerminateInstancesRequest().withInstanceIds(idsToRemove)
                System.out.println(ec2.terminateInstances(terminatedRequest))
                clearInstancesSuccess = true


            }
            catch (AmazonEC2Exception ec2Ex) {
                System.out.println(ec2Ex.getMessage())
            }
        }
        else{
            clearInstancesSuccess = true //lack of instances
        }

        if (!keysToRemove.empty) { //remove keysPairs
            for (String key: keysToRemove) {
                DeleteKeyPairRequest request = new DeleteKeyPairRequest().withKeyName(key)
                DeleteKeyPairResult response = ec2.deleteKeyPair(request)
                System.out.println(response)
            }
        }


        return  clearInstancesSuccess
    }
}
