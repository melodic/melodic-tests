import spock.lang.Specification
import groovy.time.TimeCategory
import groovy.time.TimeDuration
import groovyx.net.http.ContentType
import groovyx.net.http.RESTClient

import java.util.logging.Logger

import eu.melodic.cdo.uploader.App

import paasageService


class melodicTests extends Specification{

    def "deploymentApplicationTest"() {


        setup:
        boolean success
        //restart services
        def service = paasageService.newInstance()


        if (service.servicesRestart()) {
            System.out.println("Services restarted correctly")


            //terminated aruba instances
            def awsInstances = AWSInstances.newInstance()
            if (awsInstances.clearInstances()) {
                System.out.println("All instances with prefix aruba terminated successfully")
            }

            //read melmac.properties
            Properties properties = new Properties()
            File propertiesFile = new File('src/test/resources/melmac.properties')
            propertiesFile.withInputStream {
                properties.load(it)
            }

            String pathToTestCases = properties.getProperty("path.to.test.cases")
            String deployUrl = properties.getProperty("deploy.url")
            String processUrl = properties.getProperty("process.url")
            int timeOutSeconds = properties.getProperty("time.out.in.seconds").toInteger()

            Logger logger = Logger.getLogger("")

            //read cdo.client.properties
            System.getProperty("eu.paasage.configdir", "src/test/resources/eu.paasage.mddb.cdo.client.properties")

            //read list of test cases names
            def dlist = []
            new File(pathToTestCases).eachDir {

                //select specific folder
                //if (it.name == "FCR")

                dlist << it.name
            }

            //loop for each test case
            dlist.each {
                String applicationId = it
                logger.info("Test dla: " + it)

                //upload xmi files
                App.runAsSingle(pathToTestCases + "/" + it)

                //deploy application
                def client = new RESTClient(deployUrl)
                String todayDate = new Date().getDateTimeString()

                when:
                def response = client.post(requestContentType: ContentType.JSON,
                        headers: ['Content-Type': 'application/json'],
                        body: ['applicationId': applicationId,
                               'watermark'    : ['user'  : 'melmac',
                                                 'system': 'UI',
                                                 'date'  : todayDate,
                                                 'uuid'  : UUID.randomUUID()
                               ]
                        ])

                then:
                System.out.println(response.data.result.status)
                String processUID = response.data.processId
                assert response.status == 200
                assert response.data.result.status == "SUCCESS"

                //test of process
                when:
                String url = processUrl + processUID + "/variables"
                System.out.println("\n \nTest for: " + it + "\n")
                client = new RESTClient(url)

                success = false
                TimeDuration maxDuration = new TimeDuration(0, 0, timeOutSeconds, 0) //max czas na test
                Date startTime = new Date()

                while (!success && (TimeCategory.minus(new Date(), startTime)) <= maxDuration) {

                    System.out.println(TimeCategory.minus(new Date(), startTime))
                    when:
                    response = client.get(requestContentType: ContentType.JSON)
                    sleep(10000) //time between checking

                    then:
                    System.out.println(response.data)

                    try {
                        String cpCreationResultCode = response.data.cpCreationResultCode.value
                        System.out.println("cpCreation: " + cpCreationResultCode)


                        if (cpCreationResultCode != "SUCCESS") {
                            assert cpCreationResultCode == "SUCCESS"
                        }

                        String cpSolutionResultCode = response.data.cpSolutionResultCode.value
                        System.out.println("cpSolutionResultCode: " + cpSolutionResultCode)
                        if (cpSolutionResultCode != 'SUCCESS') {
                            assert cpSolutionResultCode == 'SUCCESS'
                        }

                        String applySolutionResultCode = response.data.applySolutionResultCode.value
                        System.out.println("applySolutionResultCode: " + applySolutionResultCode)
                        if (applySolutionResultCode != 'SUCCESS') {
                            assert applySolutionResultCode == 'SUCCESS'
                        }

                        String applicationDeploymentResultCode = response.data.applicationDeploymentResultCode.value
                        System.out.println("applicationDeploymentResultCode: " + applicationDeploymentResultCode)
                        if (applicationDeploymentResultCode != 'SUCCESS') {
                            assert applicationDeploymentResultCode == 'SUCCESS'
                        }

                        if (cpCreationResultCode == "SUCCESS") {
                            assert cpCreationResultCode == "SUCCESS"

                            if (cpSolutionResultCode == 'SUCCESS') {
                                assert cpSolutionResultCode == 'SUCCESS'

                                if (applySolutionResultCode == 'SUCCESS') {
                                    assert applySolutionResultCode == 'SUCCESS'

                                    if (applicationDeploymentResultCode == 'SUCCESS') {
                                        assert applicationDeploymentResultCode == 'SUCCESS'
                                        success = true
                                    }

                                }

                            }

                        }
                    } catch (NullPointerException | MissingPropertyException ex) {
                        System.out.println("Deployment process for " + it + " in progress.")
                    }
                }
            }
        }

        else{
            System.out.println("Fail by restart")
            success = false
        }

        assert success == true

    }

}
