Configuration:
----
1. Authentication to restart service (at choice):

    a) by key (higher priority)
        -   add melodic_id_rsa file with key to src/test/resources directory
        -   in melmac.properties file add user name (user)
        
    b) by password
        in melmac.properties file add user name (user) and password (password)


2. Credentials to AWS account

    In melmac.properties file add: 
    -   access key (access.key)
    -   secret key (secret.key)
    
    
3. Test cases are read from melodic/TestCases directory
    
    
Run test:
---
Run deploymentApplicationTest() method from melodicTest.groovy file 

