camel model FaceRecognition{
	application FaceRegonitionApp{
		version "2.5"
	}

	deployment type model DepModel{
		requirements TrainRequirements{
			provider ReqModel.ServerlessCompProvReq
			paas ReqModel.ServerlessCompPaasReq
			horizontal scale ReqModel.OnlyOneInstance
			resource ReqModel.RamReqs
			location ReqModel.LocationIE
//			image ReqModel.FaceImage
		}
		requirements RecognizeRequirements{
			provider ReqModel.ServerlessCompProvReq
			paas ReqModel.ServerlessCompPaasReq
			horizontal scale ReqModel.OnlyOneInstance
			resource ReqModel.RamReqs
			location ReqModel.LocationIE
//			image ReqModel.FaceImage
		}
		requirements WSRequirements {
			resource ReqModel.WSReq
			horizontal scale ReqModel.OnlyOneInstance
			location ReqModel.LocationIE
			provider ReqModel.ServerlessCompProvReq
			image ReqModel.FaceImage
		}
		software ComponentWS{
			requirements WSRequirements
			provided communication PortWS port 80

			script configuration ComponentWSConfiguration{
				download "echo start downloading WS >> ~/count_time.log && date +'%T' >> ~/count_time.log"
				configure "echo install script executed >> ~/count_time.log && date +'%T' >> ~/count_time.log"
				install "echo postinstall script executed >> ~/count_time.log && date +'%T' >> ~/count_time.log && printf \"<?php \\n\\$aws_key = '{{AWSKEY}}';\\n\\$aws_secret = '{{AWSSECRET}}';\\n\\$region = 'eu-west-1';\\n\\$bucket_name = 'cloudiator-faas-code';\\n\\$train_function = '$FUNCTION_NAME_TrainLambdaRequiredPort';\\n\\$recognize_function = '$FUNCTION_NAME_RecognizeLambdaRequiredPort';\\n?>\" > /var/www/html/config.php"
				start "printenv >> ~/env.txt && echo start script executed >> ~/count_time.log && date +'%T' >> ~/count_time.log"
			}
		}

		software TrainLambda{

			requirements TrainRequirements

			serverless configuration train{
				binary code URL "https://s3-eu-west-1.amazonaws.com/cloudiator-faas-code/train-f27ba518-46ba-468e-818f-d8a7e2829a36.zip"

				event configuration EventTrainConfig{
					method name "face/train"
					method type post
				}

				config param attribute TrainHandler [MetaDataModel.MELODICMetadataSchema.ContextAwareSecurityModel.Handler]: string "lambda_function.lambda_handler"

				feature TrainFunctionEnvironment {
					[MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.PaaS.Environment]
					attribute train_bucket : string "bucket cloudiator-faas-code"
					attribute train_faces : string "faces faces"
					attribute train_file1: string "file1 mmod_human_face_detector.dat"
					attribute train_file2: string "file2 dlib_face_recognition_resnet_model_v1.dat"
					attribute train_file3: string "file3 shape_predictor_5_face_landmarks.dat"
					attribute train_file4: string "file4 shape_predictor_68_face_landmarks.dat"
					attribute train_models: string "models face_recognition_models"
					attribute training_file: string "training_file trained_model.clf"
				}
			}
		}

		software RecognizeLambda{

			requirements RecognizeRequirements
			serverless configuration recognize{
				binary code URL "https://s3-eu-west-1.amazonaws.com/cloudiator-faas-code/recognize-64ee4394-15ee-46ef-ad3d-e81e0a1f059b.zip"


				event configuration EventRecognizeConfig{
					method name "face/recognize"
					method type post


				}

				config param attribute RecognizeHandler [MetaDataModel.MELODICMetadataSchema.ContextAwareSecurityModel.Handler]: string "lambda_function.lambda_handler"


				feature RecognizeFunctionEnvironment {
					[MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.PaaS.Environment]
					attribute recognize_bucket : string "bucket cloudiator-faas-code"
					attribute recognize_faces : string "faces faces"
					attribute recognize_file1: string "file1 mmod_human_face_detector.dat"
					attribute recognize_file2: string "file2 dlib_face_recognition_resnet_model_v1.dat"
					attribute recognize_file3: string "file3 shape_predictor_5_face_landmarks.dat"
					attribute recognize_file4: string "file4 shape_predictor_68_face_landmarks.dat"
					attribute recognize_models: string "models face_recognition_models"
					attribute recognize_file: string "training_file trained_model.clf"
				}
			}
		}
	}

	requirement model ReqModel{
        image requirement FaceImage ['Functionizer-AMZN_LINUX-amzn-ami-hvm-2018.03.0.20180811-x86_64-gp2' ]

		paas requirement ServerlessCompPaasReq{
			feature environment{
				[MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.PaaS.Environment]
				attribute runtime [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.PaaS.Environment.Runtime] : string "python"
			}

			feature limits{
				[MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.PaaS.Serverless.Limits]
				attribute maxExecTime [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.PaaS.Serverless.Limits.maxDuration] : int 600 UnitTemplateCamelModel.UnitTemplateModel.Seconds
				attribute memory [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.RAM]: int 2048
			}
		}

		resource requirement RamReqs {
			            feature placementApp{
            [ MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel]
                attribute placementType [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.placementType ]: string 'FAAS'
            }

			feature ramServerless{
			[ MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.RAM]
			attribute minRamLB [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.RAM.TotalMemory.totalMemoryHasMin ]
				: int 2048

			attribute maxRamLB [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.RAM.TotalMemory.totalMemoryHasMax ]
				: int 2048
			}
		}

		resource requirement WSReq{
			feature CoresWB{
				[MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.CPU]
				attribute minCores [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.CPU.hasMinNumberofCores]: int 1
				attribute maxCores [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.CPU.hasMaxNumberofCores]: int 1
			}

			feature RamWB{
				[MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.RAM]
				attribute minRamWS [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.RAM.TotalMemory.totalMemoryHasMin ]: int 1024
				attribute maxRamWS [MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Processing.RAM.TotalMemory.totalMemoryHasMax]: int 1024
			}


		}

		horizontal scale requirement OnlyOneInstance [1,1]

		provider requirement ServerlessCompProvReq{
			provider names ["aws-ec2"]

		}
		location requirement LocationIE [Locations.IE]
	}


}